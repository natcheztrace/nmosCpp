var ledger = require('../nmos-ledger');
var test = require('tape');
var express = require('express');
var bodyparser = require('body-parser');
var os = require('os');

function generatePort() {
    return Math.random() * 32768 | 0 + 32768;
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

var currentNodeIP = "current node ip";
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            currentNodeIP = address.address;
        }
    }
}

var currentPort = generatePort();
var additPort = 56864 //generatePort();

var node = new ledger.Node(null, null, "Senders Node ", "http://" + currentNodeIP + ":" + currentPort + "/", "ledger");
var store = new ledger.NodeRAMStore(node);
var nodeAPI = new ledger.NodeAPI(currentPort, store);
var addAPI = express();

addAPI.use(function(req, res, next) {
    // TODO enhance this to better supports CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, HEAD, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type, Accept");
    res.header("Access-Control-Max-Age", "3600");

    if (req.method == 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
});
addAPI.use(bodyparser.json());
addAPI.get('/', function(req, res) {
    res.json([
        "sdp/id",
        "packets/"
    ]);
});
addAPI.get('/packets/', function(req, res) {
    res.json([
        "count/",
        "turnOn/",
        "turnOff/"
    ]);
});

addAPI.get('/packets/count/', function(req, res, next) {
    var addon = require("./build/Release/addon");
    var results = addon.count();
    res.json(results);
});


addAPI.get('/sdp/video/', function(req, res, next) {
    var addon = require("./build/Release/addon");
    var results = addon.getSDP("manifest.txt");
    res.json(results);
});


addAPI.use(function(err, req, res, next) {
    if (err.status) {
        res.status(err.status).json({
            code: err.status,
            error: (err.message) ? err.message : 'Internal server error. No message available.',
            debug: (err.stack) ? err.stack : 'No stack available.'
        });
    } else {
        res.status(500).json({
            code: 500,
            error: (err.message) ? err.message : 'Internal server error. No message available.',
            debug: (err.stack) ? err.stack : 'No stack available.'
        });
    }
});
addAPI.use(function(req, res, next) {
    res.status(404).json({
        code: 404,
        error: `Could not find the requested resource '${req.path}'.`,
        debug: req.path
    });
});
var additserver = addAPI.listen(additPort, function(e) {
    var host = additserver.address().address;
    var port = additserver.address().port;
    if (e) {
        if (e.code == 'EADDRINUSE') {
            console.log('Address http://%s:%s already in use.', host, port);
            additserver.close();
        }
    } else {
        console.log('Streampunk media ledger additional node service running at http://%s:%s',
            host, port);
    }
});



//////////////////////////////////////////////// TEST SECTION ////////////////////////////////////////////////////


test('Put a SDP file', function(t) {
    var addon = require("./build/Release/addon");
    var results = addon.getSDP("manifest.txt");
    nodeAPI.putSDP("video", results);
    t.end();
});


test('Starting stores', function(t) {
    nodeAPI.init().start();
    t.end();
});


var device = new ledger.Device(null, null, "Macnica Cobra Sender", null, node.id);
var videoSource = new ledger.Source(null, null, "Macnica", "Vids-1",
    ledger.formats.video, null, null, device.id);
var audioSource = new ledger.Source(null, null, "Macnica", "Audios-1",
    ledger.formats.audio, null, null, device.id);
var audioFlow = new ledger.Flow(null, null, "Macnica", "AFlow-1",
    ledger.formats.audio, null, audioSource.id);
var videoFlow = new ledger.Flow(null, null, "Macnica", "VFlow-1",
    ledger.formats.video, null, videoSource.id);
var audioSender = new ledger.Sender(null, null, "Macnica",
    "AudS-1", audioFlow.id,
    ledger.transports.rtp_mcast, device.id, "http://" + currentNodeIP + ":" + currentPort + "/sdp/video.sdp");
var videoSender = new ledger.Sender(null, null, "Macnica",
    "VidS-1", videoFlow.id,
    ledger.transports.rtp_mcast, device.id, "http://" + currentNodeIP + ":" + currentPort + "/sdp/video.sdp");

var testResources = [device, videoSource, audioSource, audioFlow, videoFlow,
    audioSender, videoSender
];

test("Registering resources", function(t) {
    t.plan(testResources.length);
    testResources.forEach(function(r) {
        nodeAPI.putResource(r).then(t.pass, t.fail);
    });

});
