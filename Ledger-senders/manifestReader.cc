#include <node.h>
#include <fstream>
#include <v8.h>
#include <string>
#include <stdlib.h>
#include <sstream>
//#include "serious.h"

using namespace v8;


void getSDP(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  std::string filename;
  Local<Object> sdp_content = Object::New(isolate);


  if(!args[0]->IsString())
  {
    isolate->ThrowException(Exception::TypeError(
          String::NewFromUtf8(isolate, "Missing manifest path.")));
  }
  else
  {
    String::Utf8Value utfline(args[0]);
    filename = std::string(*utfline);
  }

  std::ifstream ifs (filename, std::ifstream::in);
  std::string line;
  std::string res = "";
  if (ifs.is_open())
  {
    
    while (getline(ifs, line)){
       res = res + line + "\n";
      // std::string delimiter = "=";
      // std::string key = line.substr(0, line.find(delimiter));
      // std::string value = line.substr(line.find(delimiter)+1,line.length());
      // sdp_content->Set(String::NewFromUtf8(isolate,key.c_str()), String::NewFromUtf8(isolate,value.c_str()));
    }
    ifs.close();
  }
  //
  //
  // const char *cstr = resline.c_str();
  // Local<String> result = String::NewFromUtf8(isolate, cstr);
  // Local<Number> buf = Number::New(isolate, i);
  args.GetReturnValue().Set(String::NewFromUtf8(isolate,res.c_str()));
}

void send(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, "sended"));
}

void receive(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, "received"));
}

void count(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  //start_work("host google.com");
  int res = 12;
  args.GetReturnValue().Set(Number::New(isolate, res));
}

void init(Local<Object> exports) {
  NODE_SET_METHOD(exports, "getSDP", getSDP);
  NODE_SET_METHOD(exports, "send",send);
  NODE_SET_METHOD(exports, "receive",receive);
  NODE_SET_METHOD(exports, "count", count);
}

NODE_MODULE(addon, init)
