var test = require('tape');
var http = require('http');
var url = require('url');
var os = require('os');
var ledger = require('nmos-ledger');


var mainLedgerIP = "ip should be here";
var currentNodeIP = "current node ip";
var currentSender;
var currentReceiver;

var Sender = ledger.Sender;
var Receiver = ledger.Receiver;

mainLedgerIP = process.argv[2].split("=")[1];
queryAPIPort = process.argv[3].split("=")[1];


var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            currentNodeIP = address.address;
        }
    }
}


test("Catching information about senders and receiver", function(t) {
    http.get({
        host: mainLedgerIP,
        port: queryAPIPort,
        path: '/x-nmos/query/v1.0/senders/'
    }, function(res) {
        res.on("data", function(chunk) {
            var body = JSON.parse(chunk.toString());
            currentSender = Sender.prototype.parse(body[0]);
            t.ok(currentSender.hasOwnProperty('id'), "Sender id ready now, looking for receiver...");
            console.log(currentSender);
            http.get({
                host: mainLedgerIP,
                port: queryAPIPort,
                path: '/x-nmos/query/v1.0/receivers/'
            }, function(res) {
                res.on("data", function(chunk) {
                    var body = JSON.parse(chunk.toString());
                    currentReceiver = Receiver.prototype.parse(body[0]);
                    console.log(currentReceiver);
                });
            });
        });
        t.end();
    }).on('error', function(e) {
        t.fail(e);
    });
});

test("Make a subscription on receiver node", function(t) {
    http.get({
        host: mainLedgerIP,
        port: queryAPIPort,
        path: '/x-nmos/query/v1.0/senders/'
    }, function(res) {
        res.on("data", function(chunk) {
            var body = JSON.parse(chunk.toString());
            currentSender = Sender.prototype.parse(body[0]);
            t.ok(currentSender.hasOwnProperty('id'), "Sender id ready now, looking for receiver...");
            http.get({
                host: mainLedgerIP,
                port: queryAPIPort,
                path: '/x-nmos/query/v1.0/receivers/'
            }, function(res) {
                res.on("data", function(chunk) {
                    var body = JSON.parse(chunk.toString());
                    currentReceiver = Receiver.prototype.parse(body[0]);
                    http.get({
                        host: mainLedgerIP,
                        port: queryAPIPort,
                        path: `/x-nmos/query/v1.0/devices/${currentReceiver.device_id}/`
                    }, function(res) {
                        res.on("data", function(chunk) {
                            var body = JSON.parse(chunk.toString());
                            var intentNodeId = body['node_id'];
                            http.get({
                                host: mainLedgerIP,
                                port: queryAPIPort,
                                path: `/x-nmos/query/v1.0/nodes/${intentNodeId}/`
                            }, function(res) {
                                res.on("data", function(chunk) {
                                    var body = JSON.parse(chunk.toString());
                                    var intentNodeUrl = url.parse(body["href"]);
                                    var subscribeReq = http.request({
                                        host: intentNodeUrl.hostname,
                                        port: intentNodeUrl.port,
                                        path: `/x-nmos/node/v1.0/receivers/${currentReceiver.id}/target`,
                                        method: 'PUT',
                                        headers: {
                                            'Content-Type': 'application/json',
                                            'Content-Length': currentSender.stringify().length
                                        }
                                    }, function(res) {
                                        t.equals(res.statusCode, 202, 'Connected with receiver target.');
                                    });
                                    subscribeReq.write(currentSender.stringify());
                                    subscribeReq.end();
                                    t.end();
                                })
                            });
                        })
                    });
                });
            });
        });
    });
});

test("Getting SDP file trough manifest_href from sender", function(t) {
    http.get({
        host: mainLedgerIP,
        port: queryAPIPort,
        path: '/x-nmos/query/v1.0/senders/'
    }, function(res) {
        res.on("data", function(chunk) {
            var body = JSON.parse(chunk.toString());
            currentSender = Sender.prototype.parse(body[0]);
            t.ok(currentSender.hasOwnProperty('id'), "Sender id ready now, looking for receiver...");
            var manifest = url.parse(currentSender.manifest_href);
            console.log(manifest);
            http.get({
                port: manifest.port,
                host: manifest.hostname,
                path: manifest.path
            }, function(res) {
                var data = "";
                res.on('data', function(chunk) {
                    data += chunk;
                    console.log("SDP file downloaded and parsed!");
                    console.log(data.toString());
                    t.end();
                }).on('error', function(e) {
                    t.fail(e);
                });
            });
        });
    });

});
