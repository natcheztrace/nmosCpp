var ledger = require('../nmos-ledger');
var test = require('tape');
var http = require('http');
var url = require('url');
var os = require('os');

function generatePort() {
    return Math.random() * 32768 | 0 + 32768;
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

var currentNodeIP = "current node ip";
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            currentNodeIP = address.address;
        }
    }
}
var currentPort = generatePort();
var node = new ledger.Node(null, null, "Receivers Node", "http://" + currentNodeIP + ":" + currentPort + "/", "ledger");
var store = new ledger.NodeRAMStore(node);
var nodeAPI = new ledger.NodeAPI(currentPort, store);

test('Starting stores', function(t) {
    nodeAPI.init().start();
    t.end();
});

var device = new ledger.Device(null, null, "Macnica Cobra Receiver", null, node.id);
var audioReceiver = new ledger.Receiver(null, null, "Cobra audio receiver",
    "Example of receiver", ledger.formats.audio, null, null, device.id,
    ledger.transports.rtp_mcast);
var videoReceiver = new ledger.Receiver(null, null, "Cobra video receiver",
    "Example of receiver", ledger.formats.video, null, null, device.id,
    ledger.transports.rtp_mcast);


var testResources = [device, audioReceiver, videoReceiver];

test('Registering resources', function(t) {
    t.plan(testResources.length);
    testResources.forEach(function(r) {
        nodeAPI.putResource(r).then(t.pass, t.fail)
    });
});
