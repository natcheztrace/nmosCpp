#include <node.h>
#include <fstream>
#include <v8.h>
#include <string>
#include <stdlib.h>
#include <sstream>

using namespace v8;

void handleSDP(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  String::Utf8Value utfline(args[0]);
  std::string sdp_content = std::string(*utfline);

  args.GetReturnValue().Set(String::NewFromUtf8(isolate,sdp_content.c_str()));
}

void init(Local<Object> exports) {
  NODE_SET_METHOD(exports, "handleSDP", handleSDP);
}

NODE_MODULE(addon, init)
