This project provides an example of testing Node.js project with tape and using C++ addons with Network Media Open Spec (NMOS).

## Quick guide:
###### 1) Download and unpack content of repository.
###### 2) Ctrl+Alt+T to run terminal. If you havent Nodejs, install it from https://nodejs.org/en/
###### 3) Go to folder "nmos-ledger/bin" and run main ledger server
`./nmos-ledger`
###### 3.1 ) You will need to rebuild c++ code, so install this npm packet https://github.com/nodejs/node-gyp

###### 4) Go to Ledger-senders directory, install dependecies `npm i` and run ledger with senders and flows  `node .`

###### 5) From new terminal window, go to Ledger-receivers directory, install dependencies and run ledger with receivers 
`npm i` then `node .` Now you should have 3 different ledger servers, works at the same time.
###### 6) Go to Ledger-senders folder, and run test from mainTest.js file, like `node mainTest.js ip=main_ledger_ip_here port=ledger_query_port_here`.
###### And as a final test result you should see at terminal with ledger for receivers firstly, content of the SDP file form sender, and than a string like
`"SDP file downloaded and parsed!".`

## Additional steps for CentOS

###### If you have a problem with gyp (happens on CentOS), that connected with "make" packet, you need to install devtoolset-2 gcc as described below:
`wget http://people.centos.org/tru/devtools-2/devtools-2.repo -O /etc/yum.repos.d/devtools-2.repo` then
`yum install devtoolset-2-gcc devtoolset-2-binutils devtoolset-2-gcc-c++` and finally
`scl enable devtoolset-2 bash`.
After that, `node-gyp rebuild` should works fine!

###### If you're working with apt-get PM, you need to install build-essentials, not the devtoolset-2 `sudo apt-get install build-essentials`.

###### Sometimes if yum cant install all necessary packets, you should add to file /etc/yum.conf next string
```CODE: SELECT ALL 
	http_caching=packages```

###### You could realized, that mDNS  not working on your station, then you need to make mDNS packets trustly from linux firewall, also make trustly network interface, that using by your internet connection.  Then better to reboot your system. If you have  message "cannot read property "address" " of undefine, make shure that you have connection to the internet.

